/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientsigndocument;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author nena_
 */
public class ClientSignDocument {

    private static final String USER_AGENT = "Mozilla/5.0";

    private static final String POST_URL = "http://localhost:8080/signature/api/document/sign";

    public static void main(String[] args) throws IOException {

        sendPOST();
        System.out.println("POST DONE");

    }

    private static void sendPOST() throws IOException {
        URL obj = new URL(POST_URL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");

        String jsonInputString = "{\"domain\":\"https://ditockut59.execute-api.us-east-2.amazonaws.com\",\"pathAuthorizationApi\":\"/pre/authentication/api/login\",\"pathSignatureApi\":\"/pre/signature/api/sign/pades\",\"filePathLog\":\"D:/server/FreeLance/signatureLog.log\",\"filePathToSing\":\"D:/server/FreeLance/Firma3.pdf\",\"filePathReadySing\":\"D:/server/FreeLance/firmado/Firma3.pdf\",\"filePathImageSing\":\"D:/server/FreeLance/firma-2.jpeg\",\"isWriteLog\":\"true\",\"targetSingWord\":\"Ubicacion_Firma_Digital_noBorrar\",\"securityUserToken\":\"GCC0013Q\",\"securityPasswordToken\":\"dur3i72SfHWs\",\"securityDocumentNumberToSing\":\"71720\",\"securityPasswordToSing\":\"220757P3V9\",\"securityUserTSA\":\"900373913\",\"securityPasswordTSA\":\"T3rhnAGD3Fkw2NALrKPqxRuRPsd1rNNyy8SY9O8E4URhW2QPDVToG9MAFb1vVnxyxNl/o8djE7N8h2kSRJDBeOJkmx9l117Eysy4R6N+dD4=\",\"location\":\"Bogota\",\"reason\":\"Esto es de pruebas\",\"withLTV\":\"true\",\"withStamp\":\"true\",\"visibleSignature\":\"false\",\"widthImageSing\":170,\"highImageSing\":80}";

        // For POST only - START
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(jsonInputString);
        wr.flush();
        wr.close();
        // For POST only - END

        int responseCode = con.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            StringBuffer response = readResponse(con);          
            // print result
            System.out.println(response.toString());
        } else {
            System.out.println("POST request did not work.");
        }
    }

    private static StringBuffer readResponse(HttpURLConnection con) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response;

    }

}
