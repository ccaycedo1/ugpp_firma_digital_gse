package co.gov.ugpp.signature.exception;

public class SignatureServiceException extends RuntimeException {

    public SignatureServiceException(String message) {
        super(message);
    }

    public SignatureServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
