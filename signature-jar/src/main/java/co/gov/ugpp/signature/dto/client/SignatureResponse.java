package co.gov.ugpp.signature.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignatureResponse {

    @JsonProperty("mensaje")
    private String message;

    @JsonProperty("documentoFirmado")
    private String signedPdfDocumentBase64;

    @JsonProperty("validarFirma")
    private String validateSignature;

    private Integer httpStatus;

    @JsonProperty("codigoRespuesta")
    private String codeResponse;
}
