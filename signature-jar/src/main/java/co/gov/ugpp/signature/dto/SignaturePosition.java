package co.gov.ugpp.signature.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignaturePosition {


    private Float axisX;

    private Float axisY;

    private Integer page;

    public SignaturePosition(Float axisX, Float axisY, Integer page) {
        this.axisX = axisX;
        this.axisY = axisY;
        this.page = page;
    }
}
