package co.gov.ugpp.signature.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageSignature {

    @JsonProperty("x")
    private Float axisX;

    @JsonProperty("y")
    private Float axisY;

    @JsonProperty("ancho")
    private Integer width;

    @JsonProperty("alto")
    private Integer high;

    @JsonProperty("pagina")
    private Integer page;

    @JsonProperty("imagen")
    private String imageBase64;

}
