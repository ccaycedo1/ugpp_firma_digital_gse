package co.gov.ugpp.signature.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignatureClientApiConfig {

    private String domain;

    private String pathAuthorization;

    private String pathSignature;

    public SignatureClientApiConfig() {
    }

    public SignatureClientApiConfig(String domain, String pathAuthorization, String pathSignature) {
        this.domain = domain;
        this.pathAuthorization = pathAuthorization;
        this.pathSignature = pathSignature;
    }
}
