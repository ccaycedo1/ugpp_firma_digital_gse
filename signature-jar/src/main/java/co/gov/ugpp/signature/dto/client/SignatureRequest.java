package co.gov.ugpp.signature.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignatureRequest {

    @JsonProperty("numeroDocumento")
    private String documentNumber;

    @JsonProperty("clave")
    private String password;

    @JsonProperty("usuarioTSA")
    private String userTSA;

    @JsonProperty("claveTSA")
    private String passwordTSA;

    @JsonProperty("ubicacion")
    private String location;

    @JsonProperty("razon")
    private String reason;

    @JsonProperty("conLTV")
    private Boolean withLTV;

    @JsonProperty("conEstampa")
    private Boolean withStamp;

    @JsonProperty("firmaVisible")
    private Boolean visibleSignature;

    @JsonProperty("imagenFirma")
    private ImageSignature imageSignature;

    @JsonProperty("base64")
    private String documentPDFBase64;

}
