package co.gov.ugpp.signature.componet;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

import java.io.IOException;
import java.util.List;

public class CustomPDFTextStripper extends PDFTextStripper {


    private String targetWord;
    private boolean wordFound;
    private Integer customCurrentPage;

    public Float x;
    public Float y;

    public CustomPDFTextStripper(String targetWord) throws IOException {
        super();
        this.targetWord = targetWord;
        this.wordFound = false;
        this.customCurrentPage = 0;
    }

    @Override
    protected void writeString(String text, List<TextPosition> textPositions) throws IOException {
        super.writeString(text, textPositions);
        if (text.trim().equals(targetWord.trim())) {
            wordFound = true;
            this.customCurrentPage = getCurrentPageNo();
            TextPosition firstProsition = textPositions.get(0);
            x = firstProsition.getXDirAdj();
            y = (firstProsition.getPageHeight() - firstProsition.getYDirAdj()) - 40;

        }

    }

    @Override
    protected void startPage(PDPage page) throws IOException {
        super.startPage(page);
    }

    public boolean isWordFound() {
        return wordFound;
    }

    public Integer getCustomCurrentPage() {
        return customCurrentPage;
    }

    public Float getX() {
        return x;
    }

    public Float getY() {
        return y;
    }
}
