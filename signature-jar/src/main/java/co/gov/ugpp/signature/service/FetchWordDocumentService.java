package co.gov.ugpp.signature.service;

import co.gov.ugpp.signature.componet.CustomPDFTextStripper;
import co.gov.ugpp.signature.dto.SignaturePosition;
import co.gov.ugpp.signature.exception.SignatureServiceException;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class FetchWordDocumentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchWordDocumentService.class);

    public SignaturePosition buscarPalabraEnPDF(String filePath, String targetWord) {
        try (PDDocument document = Loader.loadPDF(new File(filePath))) {
            CustomPDFTextStripper textStripper = new CustomPDFTextStripper(targetWord);
            textStripper.setSortByPosition(true);
            textStripper.getText(document);
            if (!textStripper.isWordFound()) {
                throw new SignatureServiceException("La palabra no se encontró en el PDF.");
            }

            Integer page = textStripper.getCustomCurrentPage();
            LOGGER.info(String.format(" La palabra para firmar se encontro en la pagina: %s,  coordenadas [X: %s , Y: %s]",
                    page, textStripper.getX(), textStripper.getY()));
            return new SignaturePosition(textStripper.getX(), textStripper.getY(), page);
        } catch (IOException e) {
            throw new SignatureServiceException("Error buscando las coordenadas para firmar", e);
        }
    }


}
