package co.gov.ugpp.signature.exception;

public class AuthorizationSignatureClientException extends RuntimeException {

    public AuthorizationSignatureClientException(String message) {
        super(message);
    }

    public AuthorizationSignatureClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
