package co.gov.ugpp.signature.client;

import co.gov.ugpp.signature.dto.SignatureClientApiConfig;
import co.gov.ugpp.signature.dto.client.SignatureRequest;
import co.gov.ugpp.signature.dto.client.SignatureResponse;
import co.gov.ugpp.signature.exception.SignatureClientApiException;
import co.gov.ugpp.signature.util.JsonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Objects;

public class SignatureClientApi {


    private SignatureClientApiConfig signatureConfig;

    private String token;

    public SignatureClientApi(SignatureClientApiConfig signatureConfig, String token) {
        this.signatureConfig = signatureConfig;
        this.token = token;
    }

    public SignatureResponse firmarDocumento(SignatureRequest signatureRequest) {

        HttpClient client = HttpClient.newHttpClient();
        try {
            String data = JsonUtil.objectToString(signatureRequest);

            HttpRequest request = HttpRequest.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("Authorization", String.format("Bearer %s", token))
                    .uri(URI.create(signatureConfig.getDomain() + signatureConfig.getPathSignature()))
                    .POST(HttpRequest.BodyPublishers.ofString(data))
                    .build();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            SignatureResponse result = JsonUtil.stringToClass(response.body(), SignatureResponse.class);

            if (!Objects.requireNonNull(HttpStatus.resolve(response.statusCode())).is2xxSuccessful()) {
                throw new SignatureClientApiException(String.format("Error en la respuesta del cliente de formar documento ,codigo: %s , razon: %s , detalle: %s ",
                        response.statusCode(), result.getMessage(), response.body()));
            }
            return result;
        } catch (JsonProcessingException e) {
            throw new SignatureClientApiException("Error de conversion en el cliente de firma el documento", e);
        } catch (IOException | InterruptedException e) {
            throw new SignatureClientApiException("Error en el llamado del endpoint que firma el documento ", e);
        } catch (Exception e) {
            throw new SignatureClientApiException("Otros error en el cliente firma el documento ", e);
        }
    }

}
