package co.gov.ugpp.signature.dto;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SignatureConfig {

    private String domain;

    private String pathAuthorizationApi;

    private String pathSignatureApi;

    private String filePathLog;

    private Boolean isWriteLog = true;

    private String filePathToSing;

    private String fileToSingBase64;

    private String filePathReadySing;

    private String filePathImageSing;

    private String fileImageSingBase64;

    private String targetSingWord;

    private String securityUserToken;

    private String securityPasswordToken;

    private String securityDocumentNumberToSing;

    private String securityPasswordToSing;

    private String securityUserTSA;

    private String securityPasswordTSA;

    private String location;

    private String reason;

    private Boolean withLTV = true;

    private Boolean withStamp = true;

    private Boolean visibleSignature = true;

    private Integer widthImageSing = 170;

    private Integer highImageSing = 80;


}
