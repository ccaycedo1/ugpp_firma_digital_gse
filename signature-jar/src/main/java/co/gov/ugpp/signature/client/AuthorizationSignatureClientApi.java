package co.gov.ugpp.signature.client;

import co.gov.ugpp.signature.dto.SignatureClientApiConfig;
import co.gov.ugpp.signature.dto.client.AuthorizationSignatureRequest;
import co.gov.ugpp.signature.dto.client.AuthorizationSignatureResponse;
import co.gov.ugpp.signature.exception.AuthorizationSignatureClientException;
import co.gov.ugpp.signature.util.JsonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Objects;

public class AuthorizationSignatureClientApi {

    private SignatureClientApiConfig signatureConfig;

    public AuthorizationSignatureClientApi(SignatureClientApiConfig signatureConfig) {
        this.signatureConfig = signatureConfig;
    }

    public AuthorizationSignatureResponse getAuthorization(AuthorizationSignatureRequest signatureRequest) {
        try {
            String data = JsonUtil.objectToString(signatureRequest);

            HttpRequest request = HttpRequest.newBuilder()
                    .header("Content-Type", "application/json")
                    .uri(URI.create(signatureConfig.getDomain() + signatureConfig.getPathAuthorization()))
                    .POST(HttpRequest.BodyPublishers.ofString(data))
                    .build();

            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            AuthorizationSignatureResponse result = JsonUtil.stringToClass(response.body(), AuthorizationSignatureResponse.class);

            if (!Objects.requireNonNull(HttpStatus.resolve(response.statusCode())).is2xxSuccessful()) {
                throw new AuthorizationSignatureClientException(String.format("Error en la respuesta del cliente de autorizacion,codigo: %s , razon: %s , detalle: %s ",
                        response.statusCode(), result.getDescriptionResponse(), response.body()));
            }
            return result;
        } catch (JsonProcessingException e) {
            throw new AuthorizationSignatureClientException("Error de conversion en cliente de autorizacio", e);
        } catch (IOException | InterruptedException e) {
            throw new AuthorizationSignatureClientException("Error en el llamado del app de autorizacion de la firma ", e);
        } catch (Exception e) {
            throw new AuthorizationSignatureClientException("Otros error en el cliente autorizacion de la firma ", e);
        }
    }

}
