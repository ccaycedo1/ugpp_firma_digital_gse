package co.gov.ugpp.signature;


import co.gov.ugpp.signature.dto.SignatureConfig;
import co.gov.ugpp.signature.dto.client.SignatureResponse;
import co.gov.ugpp.signature.util.JsonUtil;
import lombok.SneakyThrows;

public class SignatureClientRunTest {

    @SneakyThrows
    public static void main(String[] args) {

        SignatureConfig signatureConfig = new SignatureConfig();
        signatureConfig.setDomain("https://ditockut59.execute-api.us-east-2.amazonaws.com");
        signatureConfig.setPathAuthorizationApi("/pre/authentication/api/login");
        signatureConfig.setPathSignatureApi("/pre/signature/api/sign/pades");
        signatureConfig.setFilePathLog("D:\\server\\FreeLance\\signatureLog.log");
        signatureConfig.setFilePathToSing("D:\\server\\FreeLance\\Firma3.pdf");
        signatureConfig.setFilePathImageSing("D:\\server\\FreeLance\\firma-2.jpeg");
        signatureConfig.setFilePathReadySing("D:\\server\\FreeLance\\firmado\\Firma3.pdf");
        signatureConfig.setTargetSingWord("Ubicacion_Firma_Digital_noBorrar");
        signatureConfig.setSecurityUserToken("GCC0013Q");
        signatureConfig.setSecurityPasswordToken("dur3i72SfHWs");
        signatureConfig.setSecurityDocumentNumberToSing("71720");
        signatureConfig.setSecurityPasswordToSing("220757P3V9");
        signatureConfig.setSecurityUserTSA("900373913");
        signatureConfig.setSecurityPasswordTSA("T3rhnAGD3Fkw2NALrKPqxRuRPsd1rNNyy8SY9O8E4URhW2QPDVToG9MAFb1vVnxyxNl/o8djE7N8h2kSRJDBeOJkmx9l117Eysy4R6N+dD4=");
        signatureConfig.setLocation("Bogota");
        signatureConfig.setReason("Esto es de pruebas");
        SignatureClientRun signatureClientRun = new SignatureClientRun(signatureConfig);

        SignatureResponse signatureResponse = signatureClientRun.signDocument();
        System.out.println(JsonUtil.objectToString(signatureResponse));

    }
}
