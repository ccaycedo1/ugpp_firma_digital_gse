package co.gov.ugpp.signature.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class AuthorizationSignatureRequest {

    @NonNull
    @JsonProperty("usuario")
    private String userToken;

    @NonNull
    @JsonProperty("clave")
    private String passwordToken;

    public AuthorizationSignatureRequest(@NonNull String userToken, @NonNull String passwordToken) {
        this.userToken = userToken;
        this.passwordToken = passwordToken;
    }
}
