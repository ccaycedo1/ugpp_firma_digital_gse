package co.gov.ugpp.signature.validation;

import co.gov.ugpp.signature.dto.SignatureConfig;

import javax.validation.ConstraintViolationException;
import java.io.File;
import java.util.HashSet;


public class SignatureClienteRunValidation {

    public static void runValidationSignatureConfig(SignatureConfig signatureConfig) {
        stringValid("Domain", signatureConfig.getDomain());
        stringValid("PathAuthorizationApi", signatureConfig.getPathAuthorizationApi());
        stringValid("PathSignatureApi", signatureConfig.getPathSignatureApi());

        stringValid("FilePathLog", signatureConfig.getFilePathLog());
        stringValid("FilePathToSing", signatureConfig.getFilePathToSing());
        stringValid("FilePathReadySing", signatureConfig.getFilePathReadySing());
        stringValid("FilePathImageSing", signatureConfig.getFilePathImageSing());

        stringValid("TargetSingWord", signatureConfig.getTargetSingWord());

        stringValid("SecurityUserToken", signatureConfig.getSecurityUserToken());
        stringValid("SecurityPasswordToken", signatureConfig.getSecurityPasswordToken());
        stringValid("SecurityDocumentNumberToSing", signatureConfig.getSecurityDocumentNumberToSing());
        stringValid("SecurityPasswordToSing", signatureConfig.getSecurityPasswordToSing());
        stringValid("SecurityUserTSA", signatureConfig.getSecurityUserTSA());
        stringValid("SecurityPasswordTSA", signatureConfig.getSecurityPasswordTSA());
        stringValid("Location", signatureConfig.getLocation());
        stringValid("Reason", signatureConfig.getReason());

        existFile(signatureConfig.getFilePathLog());
        existFile(signatureConfig.getFilePathToSing());
        existFile(signatureConfig.getFilePathImageSing());
    }

    private static void existFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new ConstraintViolationException(String.format("El archivo [%s] no existe", filePath), new HashSet<>());
        }
    }

    private static void stringValid(String name, String value) {
        if (value == null || value.isEmpty()) {
            throw new ConstraintViolationException(String.format("El campo [%s] no puede ser vacio", name), new HashSet<>());
        }


    }
}
