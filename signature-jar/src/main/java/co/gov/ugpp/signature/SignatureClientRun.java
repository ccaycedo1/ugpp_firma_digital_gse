package co.gov.ugpp.signature;

import co.gov.ugpp.signature.client.AuthorizationSignatureClientApi;
import co.gov.ugpp.signature.client.SignatureClientApi;
import co.gov.ugpp.signature.dto.SignatureClientApiConfig;
import co.gov.ugpp.signature.dto.SignatureConfig;
import co.gov.ugpp.signature.dto.SignaturePosition;
import co.gov.ugpp.signature.dto.client.*;
import co.gov.ugpp.signature.exception.*;
import co.gov.ugpp.signature.service.FetchWordDocumentService;
import co.gov.ugpp.signature.util.FileUtil;
import co.gov.ugpp.signature.util.SignatureLogUtil;
import co.gov.ugpp.signature.validation.SignatureClienteRunValidation;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.validation.ConstraintViolationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SignatureClientRun {

    private final SignatureConfig signatureConfig;

    private final FetchWordDocumentService signatureService;

    private final Logger logger;

    public SignatureClientRun(SignatureConfig signatureConfig) {
        this.signatureConfig = signatureConfig;
        logger = SignatureLogUtil.LOGGER(signatureConfig.getFilePathLog(), SignatureLogUtil.class);
        try {
            SignatureClienteRunValidation.runValidationSignatureConfig(signatureConfig);
            this.signatureService = new FetchWordDocumentService();
            isWriteLog("Entra al constructor de la clase SignatureClientRun");
        } catch (ConstraintViolationException e) {
            isWriteLog(e);
            throw new SignatureClienteRunException(String.format(" Error de validacion %s", e.getMessage()), e);
        }
    }

    public void isWriteLog(Throwable throwable) {
        if (signatureConfig.getIsWriteLog()) {
            logger.log(Level.WARNING, String.format("Mensaje: %s \n traza de error :%s", throwable.getMessage(), ExceptionUtils.getStackTrace(throwable)));
        }
    }

    public void isWriteLog(String message) {
        if (signatureConfig.getIsWriteLog()) {
            logger.log(Level.INFO, message);
        }
    }


    private SignatureClientApiConfig getSignatureClientApiConfig() {
        return new SignatureClientApiConfig(signatureConfig.getDomain(),
                signatureConfig.getPathAuthorizationApi(),
                signatureConfig.getPathSignatureApi());

    }

    private String getFileToSignBase64() {
        if (signatureConfig.getFileToSingBase64() == null || signatureConfig.getFileToSingBase64().isEmpty()) {
            try {
                isWriteLog("Convierte el archivo a firmar en base 64, si el campo FileToSingBase64 es null");
                byte[] singFile = FileUtil.getFile(this.signatureConfig.getFilePathToSing());
                return Base64.encodeBase64String(singFile);
            } catch (IOException e) {
                throw new SignatureClienteRunException(String.format("Error obteniendo el archivo %s", this.signatureConfig.getFilePathToSing()), e);
            }
        }
        return signatureConfig.getFileToSingBase64();
    }

    private String getFileImageSignBase64() {
        if (signatureConfig.getFileImageSingBase64() == null || signatureConfig.getFileImageSingBase64().isEmpty()) {
            try {
                isWriteLog("Convierte la imagen de la firma en base 64, si el campo FileImageSingBase64 es null");
                byte[] singFile = FileUtil.getFile(signatureConfig.getFilePathImageSing());
                return Base64.encodeBase64String(singFile);
            } catch (IOException e) {
                throw new SignatureClienteRunException(String.format("Error obteniendo el archivo %s", signatureConfig.getFilePathImageSing()), e);
            }
        }
        return signatureConfig.getFileImageSingBase64();
    }

    private SignaturePosition getSignaturePosition() {
        isWriteLog("Entra al metodo de buscar la palabra para firmar");
        return signatureService.buscarPalabraEnPDF(signatureConfig.getFilePathToSing(),
                signatureConfig.getTargetSingWord());
    }

    private ImageSignature getImageSignature() {
        ImageSignature imageSignature = new ImageSignature();
        SignaturePosition signaturePosition = getSignaturePosition();
        imageSignature.setHigh(signatureConfig.getHighImageSing());
        imageSignature.setWidth(signatureConfig.getWidthImageSing());
        imageSignature.setAxisX(signaturePosition.getAxisX());
        imageSignature.setAxisY(signaturePosition.getAxisY());
        imageSignature.setPage(signaturePosition.getPage());
        imageSignature.setImageBase64(getFileImageSignBase64());
        isWriteLog("Construye el objecto imagenFirma");
        return imageSignature;
    }

    private SignatureRequest getSignatureRequest() {
        SignatureRequest signatureRequest = new SignatureRequest();
        signatureRequest.setImageSignature(getImageSignature());
        signatureRequest.setDocumentNumber(signatureConfig.getSecurityDocumentNumberToSing());
        signatureRequest.setPassword(signatureConfig.getSecurityPasswordToSing());
        signatureRequest.setUserTSA(signatureConfig.getSecurityUserTSA());
        signatureRequest.setPasswordTSA(signatureConfig.getSecurityPasswordTSA());
        signatureRequest.setLocation(signatureConfig.getLocation());
        signatureRequest.setReason(signatureConfig.getReason());
        signatureRequest.setWithLTV(signatureConfig.getWithLTV());
        signatureRequest.setWithStamp(signatureConfig.getWithStamp());
        signatureRequest.setVisibleSignature(signatureConfig.getVisibleSignature());
        signatureRequest.setDocumentPDFBase64(getFileToSignBase64());
        isWriteLog("Construye el objecto completo para firmar el documento");
        return signatureRequest;
    }

    private void writeSignDocument(File fileSignedDocument, byte[] signedDocument) {
        try {
            Files.write(fileSignedDocument.toPath(), signedDocument);
        } catch (IOException e) {
            throw new SignatureClienteRunException(String.format("Error escribiendo el archivo firmado %s", signatureConfig.getFilePathImageSing()), e);
        }
    }

    public String getAuthorizationToken() {
        AuthorizationSignatureClientApi client = new AuthorizationSignatureClientApi(getSignatureClientApiConfig());
        AuthorizationSignatureRequest request = new AuthorizationSignatureRequest(signatureConfig.getSecurityUserToken(), signatureConfig.getSecurityPasswordToken());
        AuthorizationSignatureResponse response = client.getAuthorization(request);
        isWriteLog("Se llama al servicio para consultar el token");
        return response.getToken();
    }

    public SignatureResponse signDocument() {
        SignatureResponse signatureResponse = new SignatureResponse();
        try {
            isWriteLog("Inicia el metodo para firmar el documento");
            SignatureClientApi signatureClientApi = new SignatureClientApi(getSignatureClientApiConfig(), getAuthorizationToken());
            signatureResponse = signatureClientApi.firmarDocumento(getSignatureRequest());
            byte[] signedDocument = Base64.decodeBase64(signatureResponse.getSignedPdfDocumentBase64());
            File fileSignedDocument = new File(signatureConfig.getFilePathReadySing());
            writeSignDocument(fileSignedDocument, signedDocument);
            isWriteLog("Firma el documento");
        } catch (AuthorizationSignatureClientException
                | SignatureClienteRunException
                | SignatureClientApiException
                | ResourceNotFoundException
                | SignatureServiceException
                | ConstraintViolationException
                e) {
            isWriteLog(e);
            signatureResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            isWriteLog(e);
        }
        return signatureResponse;
    }

}
