package co.gov.ugpp.signature;

import lombok.SneakyThrows;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SingApplication {

    @SneakyThrows
    public static void main(String[] args) {
        SpringApplication.run(SingApplication.class, args);
    }


}
