package co.gov.ugpp.signature.exception;

public class SignatureClientApiException extends RuntimeException {

    public SignatureClientApiException(String message) {
        super(message);
    }

    public SignatureClientApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
