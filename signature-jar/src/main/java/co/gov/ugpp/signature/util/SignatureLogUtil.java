package co.gov.ugpp.signature.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class SignatureLogUtil {

    public static <T> Logger LOGGER(String path, Class<T> tClass) {
        Logger logger = Logger.getLogger(String.valueOf(tClass));
        try {
            FileHandler fileHandler = new FileHandler(path);
            fileHandler.setFormatter(new FormatterCustom());
            logger.addHandler(fileHandler);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return logger;
    }


    public static void main(String... args) {

        Logger  logger = SignatureLogUtil.LOGGER("D:\\server\\FreeLance\\signatureLog.log", SignatureLogUtil.class);
    }
}
