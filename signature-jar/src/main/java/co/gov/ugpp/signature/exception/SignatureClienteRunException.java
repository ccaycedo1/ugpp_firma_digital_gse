package co.gov.ugpp.signature.exception;

public class SignatureClienteRunException extends RuntimeException {

    public SignatureClienteRunException(String message) {
        super(message);
    }

    public SignatureClienteRunException(String message, Throwable cause) {
        super(message, cause);
    }
}
