package co.gov.ugpp.signature.controller;

import co.gov.ugpp.signature.integration.WebMVCTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PingControllerTest extends WebMVCTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void ping() throws Exception {
        this.mockMvc
                .perform(get("/ping"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("pong")));
    }
}
