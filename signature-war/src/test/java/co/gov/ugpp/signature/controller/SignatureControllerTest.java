package co.gov.ugpp.signature.controller;

import co.gov.ugpp.signature.dto.SignatureConfig;
import co.gov.ugpp.signature.integration.WebMVCTest;
import co.gov.ugpp.signature.util.JsonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class SignatureControllerTest extends WebMVCTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void givenSignatureConfig_whenSendPost_thenSaveEdgeProxy() throws Exception {
        SignatureConfig signatureConfig = new SignatureConfig();
        signatureConfig.setDomain("https://ditockut59.execute-api.us-east-2.amazonaws.com");
        signatureConfig.setPathAuthorizationApi("/pre/authentication/api/login");
        signatureConfig.setPathSignatureApi("/pre/signature/api/sign/pades");
        signatureConfig.setFilePathLog("D:\\server\\FreeLance\\signatureLog.log");
        signatureConfig.setFilePathToSing("D:\\server\\FreeLance\\2023170000001111.pdf");
        signatureConfig.setFilePathImageSing("D:\\server\\FreeLance\\firma-2.jpeg");
        signatureConfig.setFilePathReadySing("D:\\server\\FreeLance\\firmado\\2023170000001111.pdf");
        signatureConfig.setTargetSingWord("Ubicacion_Firma_Digital_noBorrar");
        signatureConfig.setSecurityUserToken("GCC0013Q");
        signatureConfig.setSecurityPasswordToken("dur3i72SfHWs");
        signatureConfig.setSecurityDocumentNumberToSing("71720");
        signatureConfig.setSecurityPasswordToSing("220757P3V9");
        signatureConfig.setSecurityUserTSA("900373913");
        signatureConfig.setSecurityPasswordTSA("T3rhnAGD3Fkw2NALrKPqxRuRPsd1rNNyy8SY9O8E4URhW2QPDVToG9MAFb1vVnxyxNl/o8djE7N8h2kSRJDBeOJkmx9l117Eysy4R6N+dD4=");
        signatureConfig.setLocation("Bogota");
        signatureConfig.setReason("Esto es de pruebas");
        mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/api/document/sign")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(JsonUtil.objectToString(signatureConfig).getBytes()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


}
