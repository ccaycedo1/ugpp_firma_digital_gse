package co.gov.ugpp.signature.config;

import co.gov.ugpp.signature.dto.ApiError;
import co.gov.ugpp.signature.dto.ErrorDetail;
import co.gov.ugpp.signature.exception.ResourceNotFoundException;
import co.gov.ugpp.signature.exception.SignatureClientApiException;
import co.gov.ugpp.signature.exception.SignatureServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler({ResourceNotFoundException.class})
    protected ResponseEntity<ApiError> handleResourceNotFoundException(ResourceNotFoundException e) {
        LOGGER.error("Resource Not Found Exception: ", e);
        ApiError apiError = new ApiError("resource_not_found", e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }


    @ExceptionHandler({SignatureClientApiException.class})
    protected ResponseEntity<ApiError> handleSignatureClientApiException(SignatureClientApiException e) {
        LOGGER.error("Signature Client Api Exception: ", e);
        ApiError apiError = new ApiError("signature_client_api", e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }

    @ExceptionHandler({SignatureServiceException.class})
    protected ResponseEntity<ApiError> handleSignatureServiceException(SignatureServiceException e) {
        LOGGER.error("Signature Service Exception: ", e);
        ApiError apiError = new ApiError("signature_service", e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value());
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        List<ErrorDetail> errorDetails = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errorDetails.add(new ErrorDetail(fieldName, errorMessage));
        });
        ApiError apiError = new ApiError("validation","error validation" , HttpStatus.BAD_REQUEST.value());
        apiError.setDetails(errorDetails);
        return ResponseEntity.status(apiError.getStatus()).body(apiError);
    }

}
