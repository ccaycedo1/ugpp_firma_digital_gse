package co.gov.ugpp.signature.service;

import co.gov.ugpp.signature.dto.SignatureConfig;
import co.gov.ugpp.signature.dto.client.SignatureResponse;

public interface SignatureService {

    SignatureResponse signDocument(SignatureConfig signatureConfig);
}
