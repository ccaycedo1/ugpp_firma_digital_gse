package co.gov.ugpp.signature.service;

import co.gov.ugpp.signature.client.SignatureClientApi;
import co.gov.ugpp.signature.client.SignatureClientApiImpl;
import co.gov.ugpp.signature.componet.CustomPDFTextStripper;
import co.gov.ugpp.signature.dto.SignatureClientApiConfig;
import co.gov.ugpp.signature.dto.SignatureConfig;
import co.gov.ugpp.signature.dto.SignaturePosition;
import co.gov.ugpp.signature.dto.client.*;
import co.gov.ugpp.signature.exception.ResourceNotFoundException;
import co.gov.ugpp.signature.exception.SignatureClientApiException;
import co.gov.ugpp.signature.exception.SignatureServiceException;
import co.gov.ugpp.signature.util.FileUtil;
import co.gov.ugpp.signature.util.SignatureLogUtil;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class SignatureServiceImpl implements SignatureService {

    private SignatureConfig signatureConfig;

    private SignatureClientApi signatureClientApi;

    private Logger logger;

    public SignatureServiceImpl() {
    }


    @Override
    public SignatureResponse signDocument(SignatureConfig signatureConfig) {
        this.signatureConfig = signatureConfig;
        logger = SignatureLogUtil.LOGGER(signatureConfig.getFilePathLog(), SignatureLogUtil.class);
        signatureClientApi = new SignatureClientApiImpl(getSignatureClientApiConfig());
        SignatureResponse signatureResponse = new SignatureResponse();
        try {
            isWriteLog("Inicia el metodo para firmar el documento");
            signatureResponse = signatureClientApi.signDocument(getSignatureRequest(), getAuthorizationToken());
            byte[] signedDocument = Base64.decodeBase64(signatureResponse.getSignedPdfDocumentBase64());
            File fileSignedDocument = new File(signatureConfig.getFilePathReadySing());
            writeSignDocument(fileSignedDocument, signedDocument);
            isWriteLog("Firma el documento");
        } catch (SignatureClientApiException
                | ResourceNotFoundException
                | SignatureServiceException
                e) {
            isWriteLog(e);
            signatureResponse.setMessage(e.getMessage());
        } catch (Exception e) {
            isWriteLog(e);
        }
        return signatureResponse;

    }

    public void isWriteLog(Throwable throwable) {
        if (signatureConfig.getIsWriteLog()) {
            logger.log(Level.WARNING, String.format("Mensaje: %s \n traza de error :%s", throwable.getMessage(), ExceptionUtils.getStackTrace(throwable)));
        }
    }

    public void isWriteLog(String message) {
        if (signatureConfig.getIsWriteLog()) {
            logger.log(Level.INFO, message);
        }
    }


    private SignatureClientApiConfig getSignatureClientApiConfig() {
        return new SignatureClientApiConfig(signatureConfig.getDomain(),
                signatureConfig.getPathAuthorizationApi(),
                signatureConfig.getPathSignatureApi());

    }

    private String getFileToSignBase64() {
        if (signatureConfig.getFileToSingBase64() == null || signatureConfig.getFileToSingBase64().isEmpty()) {
            try {
                validFilePathToSing();
                isWriteLog("Convierte el archivo a firmar en base 64, si el campo FileToSingBase64 es null");
                byte[] singFile = FileUtil.getFile(this.signatureConfig.getFilePathToSing());
                return Base64.encodeBase64String(singFile);
            } catch (IOException e) {
                throw new SignatureServiceException(String.format("Error obteniendo el archivo %s", this.signatureConfig.getFilePathToSing()), e);
            }
        }
        return signatureConfig.getFileToSingBase64();
    }

    private void validFilePathToSing() {
        if (signatureConfig.getFilePathToSing() == null || signatureConfig.getFilePathToSing().isEmpty()) {
            throw new SignatureServiceException("FilePathToSing mensaje : no puede estar vacio ni nulo");
        }
    }

    private String getFileImageSignBase64() {
        if (signatureConfig.getFileImageSingBase64() == null || signatureConfig.getFileImageSingBase64().isEmpty()) {
            try {
                validFiledImage();
                isWriteLog("Convierte la imagen de la firma en base 64, si el campo FileImageSingBase64 es null");
                byte[] singFile = FileUtil.getFile(signatureConfig.getFilePathImageSing());
                return Base64.encodeBase64String(singFile);
            } catch (IOException e) {
                throw new SignatureServiceException(String.format("Error obteniendo el archivo %s", signatureConfig.getFilePathImageSing()), e);
            }
        }
        return signatureConfig.getFileImageSingBase64();
    }

    private void validFiledImage() {
        if (signatureConfig.getFilePathImageSing() == null || signatureConfig.getFilePathImageSing().isEmpty()) {
            throw new SignatureServiceException("FilePathImageSing mensaje : no puede estar vacio ni nulo");
        }
    }

    private SignaturePosition getSignaturePosition() {
        isWriteLog("Entra al metodo de buscar la palabra para firmar");
        return buscarPalabraEnPDF(signatureConfig.getFilePathToSing(),
                signatureConfig.getTargetSingWord());
    }

    private ImageSignature getImageSignature() {
        String msm = "se firma el documento SIN estampa";
        ImageSignature imageSignature = new ImageSignature();
        SignaturePosition signaturePosition = getSignaturePosition();
        signatureConfig.setWithStamp(signaturePosition.getIsWordFound());
        signatureConfig.setVisibleSignature(signaturePosition.getIsWordFound());
        if (signaturePosition.getIsWordFound()) {
            imageSignature.setAxisX(signaturePosition.getAxisX());
            imageSignature.setAxisY(signaturePosition.getAxisY());
            imageSignature.setPage(signaturePosition.getPage());
            msm = "se firma el documento CON estampa";
        }
        imageSignature.setHigh(signatureConfig.getHighImageSing());
        imageSignature.setWidth(signatureConfig.getWidthImageSing());
        imageSignature.setImageBase64(getFileImageSignBase64());
        isWriteLog("Construye el objecto imagenFirma : " + msm);
        return imageSignature;
    }

    private SignatureRequest getSignatureRequest() {
        SignatureRequest signatureRequest = new SignatureRequest();
        signatureRequest.setImageSignature(getImageSignature());
        signatureRequest.setDocumentNumber(signatureConfig.getSecurityDocumentNumberToSing());
        signatureRequest.setPassword(signatureConfig.getSecurityPasswordToSing());
        signatureRequest.setUserTSA(signatureConfig.getSecurityUserTSA());
        signatureRequest.setPasswordTSA(signatureConfig.getSecurityPasswordTSA());
        signatureRequest.setLocation(signatureConfig.getLocation());
        signatureRequest.setReason(signatureConfig.getReason());
        signatureRequest.setWithLTV(signatureConfig.getWithLTV());
        signatureRequest.setWithStamp(signatureConfig.getWithStamp());
        signatureRequest.setVisibleSignature(signatureConfig.getVisibleSignature());
        signatureRequest.setDocumentPDFBase64(getFileToSignBase64());
        isWriteLog("Construye el objecto completo para firmar el documento");
        return signatureRequest;
    }

    private void writeSignDocument(File fileSignedDocument, byte[] signedDocument) {
        try {
            Files.write(fileSignedDocument.toPath(), signedDocument);
        } catch (IOException e) {
            throw new SignatureServiceException(String.format("Error escribiendo el archivo firmado %s , detalle %s",
                    signatureConfig.getFilePathImageSing(), e.getMessage()), e);
        }
    }

    private String getAuthorizationToken() {
        AuthorizationSignatureRequest request = new AuthorizationSignatureRequest(signatureConfig.getSecurityUserToken(), signatureConfig.getSecurityPasswordToken());
        AuthorizationSignatureResponse response = signatureClientApi.getAuthorizationToken(request);
        isWriteLog("Se llama al servicio para consultar el token");
        return response.getToken();
    }

    private SignaturePosition buscarPalabraEnPDF(String filePath, String targetWord) {
        try (PDDocument document = Loader.loadPDF(new File(filePath))) {
            CustomPDFTextStripper textStripper = new CustomPDFTextStripper(targetWord);
            textStripper.setSortByPosition(true);
            textStripper.getText(document);
            if (!textStripper.isWordFound()) {
                isWriteLog("La palabra NO se encontró en el PDF.");
            }

            Integer page = textStripper.getCustomCurrentPage();
            isWriteLog(String.format(" La palabra para firmar se encontro en la pagina: %s,  coordenadas [X: %s , Y: %s]",
                    page, textStripper.getX(), textStripper.getY()));
            return new SignaturePosition(textStripper.isWordFound(), textStripper.getX(), textStripper.getY(), page);
        } catch (IOException e) {
            throw new SignatureServiceException("Error buscando las coordenadas para firmar, detalle " + e.getMessage(), e);
        }
    }

}
