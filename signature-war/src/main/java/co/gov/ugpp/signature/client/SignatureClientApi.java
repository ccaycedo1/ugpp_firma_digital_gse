package co.gov.ugpp.signature.client;

import co.gov.ugpp.signature.dto.client.AuthorizationSignatureRequest;
import co.gov.ugpp.signature.dto.client.AuthorizationSignatureResponse;
import co.gov.ugpp.signature.dto.client.SignatureRequest;
import co.gov.ugpp.signature.dto.client.SignatureResponse;
import co.gov.ugpp.signature.exception.SignatureClientApiException;

public interface SignatureClientApi {

    AuthorizationSignatureResponse getAuthorizationToken(AuthorizationSignatureRequest signatureRequest) throws SignatureClientApiException;

    SignatureResponse signDocument(SignatureRequest signatureRequest, String token) throws SignatureClientApiException;
}
