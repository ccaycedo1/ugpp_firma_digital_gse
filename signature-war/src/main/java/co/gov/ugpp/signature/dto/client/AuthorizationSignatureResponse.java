package co.gov.ugpp.signature.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorizationSignatureResponse {


    @JsonProperty("token")
    private String token;

    @JsonProperty("expiracion")
    private String expiration;

    private Integer httpStatus;

    @JsonProperty("codigoRespuesta")
    private String codeResponse;

    @JsonProperty("descripcionRespuesta")
    private String descriptionResponse;

    @JsonProperty("detalleRespuesta")
    private String detailResponse;

}

