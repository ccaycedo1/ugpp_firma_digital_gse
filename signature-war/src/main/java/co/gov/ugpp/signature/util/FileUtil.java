package co.gov.ugpp.signature.util;

import co.gov.ugpp.signature.exception.ResourceNotFoundException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileUtil {

    public static byte[] getFile(String filePath) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            throw new ResourceNotFoundException(String.format("El archivo [%s] no existe", filePath));
        }
        return Files.readAllBytes(file.toPath());
    }
}
