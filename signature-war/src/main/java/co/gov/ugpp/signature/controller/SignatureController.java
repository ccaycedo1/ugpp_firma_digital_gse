package co.gov.ugpp.signature.controller;

import co.gov.ugpp.signature.dto.SignatureConfig;
import co.gov.ugpp.signature.dto.client.SignatureResponse;
import co.gov.ugpp.signature.service.SignatureService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class SignatureController {

    private final SignatureService signatureService;

    public SignatureController(SignatureService signatureService) {
        this.signatureService = signatureService;
    }

    @PostMapping(
            value = "/api/document/sign",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SignatureResponse> add(@Valid @RequestBody SignatureConfig signatureConfig) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(signatureService.signDocument(signatureConfig));
    }


}
