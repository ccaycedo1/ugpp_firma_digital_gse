package co.gov.ugpp.signature.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDetail  {

    private String field;

    private String description;

    public ErrorDetail() {
    }

    public ErrorDetail(String field, String description) {
        this.field = field;
        this.description = description;
    }
}
