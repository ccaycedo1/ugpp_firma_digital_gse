package co.gov.ugpp.signature.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageSignature {

    @JsonProperty("x")
    private Float axisX = 0F;

    @JsonProperty("y")
    private Float axisY = 0F;

    @JsonProperty("ancho")
    private Integer width;

    @JsonProperty("alto")
    private Integer high;

    @JsonProperty("pagina")
    private Integer page = 1;

    @JsonProperty("imagen")
    private String imageBase64;

}