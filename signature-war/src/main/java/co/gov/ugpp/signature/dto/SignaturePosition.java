package co.gov.ugpp.signature.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignaturePosition {

    private Boolean isWordFound = true;

    private Float axisX;

    private Float axisY;

    private Integer page;

    public SignaturePosition() {
    }

    public SignaturePosition(Boolean isWordFound, Float axisX, Float axisY, Integer page) {
        this.isWordFound = isWordFound;
        this.axisX = axisX;
        this.axisY = axisY;
        this.page = page;
    }
}