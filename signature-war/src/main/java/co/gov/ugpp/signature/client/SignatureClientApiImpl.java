package co.gov.ugpp.signature.client;

import co.gov.ugpp.signature.dto.SignatureClientApiConfig;
import co.gov.ugpp.signature.dto.client.AuthorizationSignatureRequest;
import co.gov.ugpp.signature.dto.client.AuthorizationSignatureResponse;
import co.gov.ugpp.signature.dto.client.SignatureRequest;
import co.gov.ugpp.signature.dto.client.SignatureResponse;
import co.gov.ugpp.signature.exception.SignatureClientApiException;
import co.gov.ugpp.signature.util.JsonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Objects;

public class SignatureClientApiImpl implements SignatureClientApi {

    private SignatureClientApiConfig signatureConfig;

    public SignatureClientApiImpl(SignatureClientApiConfig signatureConfig) {
        this.signatureConfig = signatureConfig;
    }

    private HttpRequest.Builder configHttpRequestAuthorization() {
        return HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .uri(URI.create(signatureConfig.getDomain() + signatureConfig.getPathAuthorization()));
    }

    public AuthorizationSignatureResponse getAuthorizationToken(AuthorizationSignatureRequest signatureRequest) {
        try {
            String data = JsonUtil.objectToString(signatureRequest);

            HttpRequest request = configHttpRequestAuthorization()
                    .POST(HttpRequest.BodyPublishers.ofString(data))
                    .build();

            HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());

            AuthorizationSignatureResponse result = JsonUtil.stringToClass(response.body(), AuthorizationSignatureResponse.class);

            if (!Objects.requireNonNull(HttpStatus.resolve(response.statusCode())).is2xxSuccessful()) {
                throw new SignatureClientApiException(String.format("Error en la respuesta del cliente de autorizacion,codigo: %s , razon: %s , detalle: %s ",
                        response.statusCode(), result.getDescriptionResponse(), response.body()));
            }
            return result;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new SignatureClientApiException("Error de parseo en el metodo de consultar el token autorizado , detalle:" + e.getMessage(), e);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new SignatureClientApiException("Error en el llamado del endpoint para consultar el token autorizado, detalle: " + e.getMessage(), e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SignatureClientApiException("Otros error metodo de consultar el token autorizado, detalle: " + e.getMessage(), e);
        }
    }

    private HttpRequest.Builder configHttpRequestSignDocument(String token) {
        return HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", String.format("Bearer %s", token))
                .uri(URI.create(signatureConfig.getDomain() + signatureConfig.getPathSignature()));
    }

    @Override
    public SignatureResponse signDocument(SignatureRequest signatureRequest, String token) throws SignatureClientApiException {
        try {
            String data = JsonUtil.objectToString(signatureRequest);

            HttpRequest request = configHttpRequestSignDocument(token)
                    .POST(HttpRequest.BodyPublishers.ofString(data))
                    .build();

            HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());

            SignatureResponse result = JsonUtil.stringToClass(response.body(), SignatureResponse.class);

            if (!Objects.requireNonNull(HttpStatus.resolve(response.statusCode())).is2xxSuccessful()) {
                throw new SignatureClientApiException(String.format("Error en la respuesta del cliente de formar documento ,codigo: %s , razon: %s , detalle: %s ",
                        response.statusCode(), result.getMessage(), response.body()));
            }
            return result;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new SignatureClientApiException("Error de parseo en el metodo de consultar de firmar el documento, detalle:" + e.getMessage(), e);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new SignatureClientApiException("Error en el llamado del endpoint para firmar el documento, detalle:" + e.getMessage(), e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SignatureClientApiException("Otros error en el metodo de firmar el documento, detalle:" + e.getMessage(), e);
        }
    }


}
