package co.gov.ugpp.signature.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignatureConfig {

    @NotBlank
    private String domain;

    @NotBlank
    private String pathAuthorizationApi;

    private String pathSignatureApi;

    @NotBlank
    private String filePathLog;

    private Boolean isWriteLog = true;

    private String filePathToSing;

    private String fileToSingBase64;

    @NotBlank
    private String filePathReadySing;

    private String filePathImageSing;

    private String fileImageSingBase64;

    @NotBlank
    private String targetSingWord;

    @NotBlank
    private String securityUserToken;

    @NotBlank
    private String securityPasswordToken;

    @NotBlank
    private String securityDocumentNumberToSing;

    @NotBlank
    private String securityPasswordToSing;

    @NotBlank
    private String securityUserTSA;

    @NotBlank
    private String securityPasswordTSA;

    @NotBlank
    private String location;

    @NotBlank
    private String reason;

    @NotNull
    private Boolean withLTV = true;

    @NotNull
    private Boolean withStamp = true;

    @NotNull
    private Boolean visibleSignature = true;

    @NotNull
    private Integer widthImageSing = 170;

    @NotNull
    private Integer highImageSing = 80;
}

